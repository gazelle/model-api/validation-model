package net.ihe.gazelle.modelapi.validation.business;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MetadataTest {
    /**
     * Test :  Testing the constructor of Metadata class
     */
    @Test
    void testCreateAMetadata() {
        try {
            // Create a metadata with mandatory parameter
            Metadata metadata = new Metadata("ProfileID", "123456789");
            Assertions.assertEquals("ProfileID", metadata.getName(), "Name shall be : ProfileID");
            Assertions.assertEquals("123456789", metadata.getValue(), "Value shall be 123456789");

            // Create a meta without mandatory data

            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> new Metadata(null, null), "Expected to " +
                    "throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("Name can not be null"), "The constructor shall throw an exception");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Testing all possibilities of Equals methods
     */
    @Test
    void testCompareMetadata() {
        try {
            Metadata metadata = new Metadata("ProfileID", "123456789");
            Metadata metadataBis = new Metadata("ProfileID", "123456789");
            Metadata metadataDif = new Metadata("ProfileID2", "123456789");
            Metadata metadataDif2 = new Metadata("ProfileID", null);
            Metadata metadataDif3 = new Metadata("ProfileID", null);

            Assertions.assertEquals(metadata, metadataBis, "Shall be equal");

            Assertions.assertEquals(metadata, metadata, "Shall be equal");

            Assertions.assertFalse(metadata.equals(null), "Shall be false ");
            Assertions.assertFalse(metadata.equals("test"), "shall be different");

            Assertions.assertNotEquals(metadata, metadataDif, "shall be different");
            Assertions.assertNotEquals(metadata, metadataDif2, "shall be different");

            Assertions.assertEquals(metadataDif2, metadataDif3, "Shall be equal");

            Assertions.assertNotEquals(metadataDif2, metadata, "shall be different");
            Assertions.assertEquals(metadataDif3, metadataDif3, "Shall be equal");


        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }

    }

    /**
     * Test : Hash method
     */
    @Test
    void testHashMethodEqual() {
        try {

            Metadata metadata = new Metadata("ProfileID", "123456789");
            Metadata metadataBis = new Metadata("ProfileID", "123456789");
            Assertions.assertEquals(metadata.hashCode(), metadataBis.hashCode(), "shall be equal");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }
    /**
     * Test : Hash method
     */
    @Test
    void testHashMethodNotEqual() {
        try {

            Metadata metadata = new Metadata("ProfileID", "123456789");
            Metadata metadataBis = new Metadata("ProfileID", null);
            Assertions.assertNotEquals(metadata.hashCode(), metadataBis.hashCode(), "shall be not equal");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }
}