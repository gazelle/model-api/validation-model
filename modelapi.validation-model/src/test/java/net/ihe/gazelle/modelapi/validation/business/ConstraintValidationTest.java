package net.ihe.gazelle.modelapi.validation.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class ConstraintValidationTest {

    private static final String ID1 = "ID_test1";
    private static final String ID2 = "ID_test2";

    private static final String constraintType1 = "length";
    private static final String constraintType2 = "test";

    private static final String constraintDescription1 = "description1 ";
    private static final String constraintDescription2 = "description2 ";

    private static final String locationInValidatedObject1 = "location1";
    private static final String locationInValidatedObject2 = "location2";

    private static final String valueInValidatedObject1 = "value in Object";
    private static final String valueInValidatedObject2 = "value in Object2";

    /**
     * Test : Create a Mandatory PASSED constraint
     */
    @Covers(requirements = {"VAL-20", "VAL-19"})
    @Test
    void testCreateNewMandatoryPassedConstraint() {
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.MANDATORY,
                ValidationTestResult.PASSED);
        Assertions.assertEquals(SeverityLevel.INFO, constraintValidation.getSeverity(), "Severity Level shall be INFO");

    }

    /**
     * Test : Create a Recommended PASSED constraint
     */
    @Covers(requirements = {"VAL-20", "VAL-19"})
    @Test
    void testCreateNewRecommendedPassedConstraint() {
        ConstraintValidation constraintValidation2 = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertEquals(SeverityLevel.INFO, constraintValidation2.getSeverity(), "Severity Level shall be INFO");
    }

    /**
     * Test : Create a Permitted PASSED constraint
     */
    @Covers(requirements = {"VAL-20", "VAL-19"})
    @Test
    void testCreateNewPermittedPassedConstraint() {
            ConstraintValidation constraintValidation3 = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.PERMITTED,
                    ValidationTestResult.PASSED);
            Assertions.assertEquals(SeverityLevel.INFO, constraintValidation3.getSeverity(), "Severity Level shall be INFO");
    }


    /**
     * Test : Create a Failed Mandatory constraint
     */
    @Covers(requirements = {"VAL-24", "VAL-18"})
    @Test
    void testCreateNewConstraintMandatoryFailed() {
        List<String> tmp = new ArrayList<>();
        tmp.add("test");
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.MANDATORY,
                ValidationTestResult.FAILED);
        Assertions.assertEquals(SeverityLevel.ERROR, constraintValidation.getSeverity(), "Severity Level shall be ERROR");

    }

    /**
     * Test : Create a Failed Recommended constraint
     */
    @Covers(requirements = "VAL-25")
    @Test
    void testCreateNewConstraintRecommendedFailed() {
        List<String> tmp = new ArrayList<>();
        tmp.add("test");
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.FAILED);
        Assertions.assertEquals(SeverityLevel.WARNING, constraintValidation.getSeverity(), "Severity Level shall be WARNING");
    }

    /**
     * Test : Create a Failed Permitted constraint
     */
    @Covers(requirements = "VAL-26")
    @Test
    void testCreateNewConstraintPermittedFailed() {
        List<String> tmp = new ArrayList<>();
        tmp.add("test");
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.PERMITTED,
                ValidationTestResult.FAILED);
        Assertions.assertEquals(SeverityLevel.INFO, constraintValidation.getSeverity(), "Severity Level shall be WARNING");
    }


    /**
     * Test : create without mandatory parameter
     */
    @Covers(requirements = {"VAL-16", "VAL-30"})
    @Test
    void testCreateConstraintValidationWithoutDescription() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> new ConstraintValidation(ID1,
                constraintType1, null, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.FAILED), "Expected to throw, but it didn't");
        Assertions.assertTrue(thrown.getMessage().contains("constraintDescription can not be null"), "It shall throw an exception");
    }

    /**
     * Test : add validation Exception
     */
    @Covers(requirements = "VAL-21")
    @Test
    void testAddValidationException() {
        List<String> tmp = new ArrayList<>();
        tmp.add("test");
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.FAILED);
        constraintValidation.addUnexpectedError(new IllegalArgumentException());
        Assertions.assertEquals(ValidationTestResult.UNDEFINED, constraintValidation.getTestResult(), "The testResult shall be UNDEFINED if an " +
                "exception is added to a constraint");
    }

    /**
     * Test : Copy a Constraint object
     */
    @Covers(requirements = "VAL-28")
    @Test
    void testCopyAConstraint() {
        List<String> tmp = new ArrayList<>();
        tmp.add("test");
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.FAILED);

        ConstraintValidation constraintValidationCopy = new ConstraintValidation(constraintValidation);

        Assertions.assertEquals(constraintValidation, constraintValidationCopy);
        Assertions.assertEquals(constraintValidation.hashCode(), constraintValidationCopy.hashCode());
    }


    /**
     * Test : add null validation Exception
     */
    @Test
    void testAddNullListValidationException() {
        List<String> tmp = new ArrayList<>();
        tmp.add("test");
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.FAILED);
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> constraintValidation.addUnexpectedErrors(null), "Expected to throw, but it didn't");
        Assertions.assertTrue(thrown.getMessage().contains("Exception list to add cannot be null"));

    }

    /**
     * Test : add validation Exception List
     */
    @Test
    void testAddListValidationException() {
        List<String> tmp = new ArrayList<>();
        tmp.add("test");
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.FAILED);
        List<UnexpectedError> exceptions = new ArrayList<>();
        exceptions.add(new UnexpectedError(new IllegalArgumentException("Name1")));
        exceptions.add(new UnexpectedError(new IllegalArgumentException("Name2")));
        exceptions.add(new UnexpectedError(new IllegalArgumentException("Name3")));
        exceptions.add(new UnexpectedError(new IllegalArgumentException("Name4")));
            constraintValidation.addUnexpectedErrors(exceptions);
            Assertions.assertEquals(4, constraintValidation.getUnexpectedErrors().size(), "The exceptions shall contain 4 items");
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, constraintValidation.getTestResult(), "Shall be UNDEFINED");

    }

    /**
     * test if we can not create with a mandatory parameter
     */
    @Covers(requirements = "VAL-23")
    @Test
    void testCreatWithOutPriorityLevel() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> new ConstraintValidation(ID1,
                constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, null,
                ValidationTestResult.FAILED), "Expected to throw, but it didn't");
        Assertions.assertTrue(thrown.getMessage().contains("priority can not be null"));
    }

    /**
     * test if we can not create with a mandatory parameter
     */
    @Test
    void testCreateWithoutValidationTestResult() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> new ConstraintValidation(ID1,
                constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                null), "Expected to throw, but it didn't");
        Assertions.assertTrue(thrown.getMessage().contains("testResult can not be null"));
    }

    /**
     * Test : Compare  with Itself, null and different class
     */
    @Test
    void testEqualsMethod() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertEquals(constraintValidationExpected, constraintValidationExpected, "Shall be equal, Same object");
        Assertions.assertFalse(constraintValidationExpected.equals(null), "Shall not be equal");
        Assertions.assertFalse(constraintValidationExpected.equals("Test"), "Shall not be equal");
    }

    /**
     * ***************************************************************************************
     * Test : Compare on the field constraintID
     */
    @Test
    void testCompareConstraintID() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);

        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertEquals(constraintValidationExpected, constraintValidationCompare, "Shall be equal due to ConstraintID");
    }

    /**
     * Test : Compare on the field constraintID, one is null
     */
    @Test
    void testCompareConstraintID2() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);

        ConstraintValidation constraintValidationCompare = new ConstraintValidation(null, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to ConstraintID");
        Assertions.assertNotEquals(constraintValidationCompare, constraintValidationExpected, "Shall not be equal due to ConstraintID");
    }

    /**
     * Test : Compare on the field constraintID, both Attributes are null
     */
    @Test
    void testCompareConstraintID3() {
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(null, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);

        ConstraintValidation constraintValidationExpected = new ConstraintValidation(null, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to ConstraintID");
    }

    /**
     * ***************************************************************************************
     * Test : Compare on the field constraint Type same
     */
    @Test
    void testCompareConstraintType() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertEquals(constraintValidationExpected, constraintValidationCompare, "Shall  be equal due to ConstraintType");

    }

    /**
     * Test : Compare on the field constraint Type different
     */
    @Test
    void testCompareConstraintType2() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType2, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to ConstraintType");
        Assertions.assertNotEquals(constraintValidationCompare, constraintValidationExpected, "Shall not be equal due to ConstraintType");
    }

    /**
     * Test : Compare on the field constraint Type one null
     */
    @Test
    void testCompareConstraintType3() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, null, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to ConstraintType");
        Assertions.assertNotEquals(constraintValidationCompare, constraintValidationExpected, "Shall not be equal due to ConstraintType");
    }

    /**
     * Test : Compare on the field constraint Type both null
     */
    @Test
    void testCompareConstraintType4() {
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, null, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, null, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertEquals(constraintValidationExpected, constraintValidationCompare, "Shall  be equal due to ConstraintType");

    }

    /**
     * ***************************************************************************************
     * Test : Compare on the field constraint Description
     */
    @Test
    void testCompareConstraintDescription() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription2, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to ConstraintDescription");
    }

    /**
     * ***************************************************************************************
     * Test : Compare on the field locationInValidatedObject  same
     */
    @Test
    void testCompareLocationInValidatedObject() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertEquals(constraintValidationExpected, constraintValidationCompare, "Shall  be equal due to " +
                "locationInValidatedObject");

    }

    /**
     * Test : Compare on the field locationInValidatedObject different
     */
    @Test
    void testCompareLocationInValidatedObject2() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject2, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to " +
                "locationInValidatedObject");
        Assertions.assertNotEquals(constraintValidationCompare, constraintValidationExpected, "Shall not be equal due to " +
                "locationInValidatedObject");
    }

    /**
     * Test : Compare on the field locationInValidatedObject one null
     */
    @Test
    void testCompareLocationInValidatedObject3() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                null, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to " +
                "locationInValidatedObject");
        Assertions.assertNotEquals(constraintValidationCompare, constraintValidationExpected, "Shall not be equal due to " +
                "locationInValidatedObject");

    }

    /**
     * Test : Compare on the field locationInValidatedObject both null
     */
    @Test
    void testCompareLocationInValidatedObject4() {
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                null, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                null, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertEquals(constraintValidationExpected, constraintValidationCompare, "Shall  be equal due to locationInValidatedObject");
    }

    /**
     * ***************************************************************************************
     * Test : Compare on the field locationInValidatedObject
     */
    @Test
    void testCompareValueInValidatedObject() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertEquals(constraintValidationExpected, constraintValidationCompare, "Shall  be equal due to valueInValidatedObject");
    }

    /**
     * Test : Compare on the field locationInValidatedObject different
     */
    @Test
    void testCompareValueInValidatedObject1() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject2, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall  be equal due to valueInValidatedObject");
    }

    /**
     * Test : Compare on the field locationInValidatedObject one null
     */
    @Test
    void testCompareValueInValidatedObject2() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, null, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to valueInValidatedObject");
        Assertions.assertNotEquals(constraintValidationCompare, constraintValidationExpected, "Shall not be equal due to valueInValidatedObject");
    }

    /**
     * Test : Compare on the field locationInValidatedObject both are null
     */
    @Test
    void testCompareValueInValidatedObject3() {
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, null, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);

        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, null, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertEquals(constraintValidationExpected, constraintValidationCompare, "Shall  be equal due to valueInValidatedObject");
    }


    /**
     * Test : Compare on the field assertionIDs
     */
    @Test
    void testCompareAssertionIDs() {
        List<String> tmp = new ArrayList<>();
        tmp.add("test");
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to assertionIDs");
    }

    /**
     * Test : Compare on the field exceptions
     */
    @Test
    void testCompareExceptions() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        constraintValidationCompare.addUnexpectedError(new IllegalArgumentException("name"));
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to exceptions");
    }

    /**
     * Test : Compare on the field testResult
     */
    @Test
    void testCompareTestResult() {
        List<String> tmp = new ArrayList<>();
        tmp.add("test");
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.PERMITTED,
                ValidationTestResult.FAILED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.PERMITTED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to testResult");
    }

    /**
     * Test : Compare on the field severity
     */
    @Covers(requirements = "VAL-22")
    @Test
    void testCompareSeverity() {
        List<String> tmp = new ArrayList<>();
        tmp.add("test");
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.PERMITTED,
                ValidationTestResult.FAILED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, tmp, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.FAILED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to severity");
    }

    /**
     * Test : Compare on the field priority
     */
    @Test
    void testComparePriority() {
        ConstraintValidation constraintValidationExpected = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.PERMITTED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidationCompare = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidationExpected, constraintValidationCompare, "Shall not be equal due to priority");
    }

    /**
     * Test : Test HashCode method
     */
    @Test
    void testHashCodeMethod() {
        ConstraintValidation constraintValidation = new ConstraintValidation(ID2, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidation1 = new ConstraintValidation(null, null, constraintDescription1, null,
                null, null, null, null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        Assertions.assertNotEquals(constraintValidation.hashCode(), constraintValidation1.hashCode(), "HashCode shall be different");
    }
}
