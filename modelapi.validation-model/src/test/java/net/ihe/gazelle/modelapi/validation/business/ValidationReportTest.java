package net.ihe.gazelle.modelapi.validation.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class ValidationReportTest {
    private static final String disclaimer = "TestValue";
    private static final Date validationDateTime = new Date();
    private static final String validationServiceName = "TestValue";
    private static final String validationServiceVersion = "TestValue";
    private static final String validatorID = "TestValue";
    private static final String ID1 = "ID_test1";
    private static final String constraintType1 = "length";
    private static final String constraintDescription1 = "description1 ";
    private static final String locationInValidatedObject1 = "location1";
    private static final String valueInValidatedObject1 = "value in Object";


    /**
     * Test : Constructor of a Validation Report
     */
    @Covers(requirements = "VAL-36")
    @Test
    void testGenerateValidationOverview() {
        try {
            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> new ValidationReport("Report1", null),
                    "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("validationOverview can not be null"));

            ValidationReport report = new ValidationReport("Report1", createOverview());

            Assertions.assertEquals(ValidationTestResult.UNDEFINED, report.getValidationOverallResult(), "Shall be " +
                    "Undefined");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Setter and Getter for the field uuid
     */
    @Test
    void testSetterGetterUuid() {
        try {
            ValidationReport report = new ValidationReport("Report1", createOverview());

            Assertions.assertEquals(ValidationTestResult.UNDEFINED, report.getValidationOverallResult(), "Shall be " +
                    "Undefined");
            Assertions.assertEquals("Report1", report.getUuid(), "The uuid shall be : Report1");
            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> report.setUuid(null),
                    "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("uuid can not be null"), "uuid can not be null");
            report.setUuid("Report2");
            Assertions.assertEquals("Report2", report.getUuid(), "The uuid shall have changed to  Report2");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }


    /**
     * Test : Getter and Setter of Validation Overview
     */
    @Test
    void testGetterSetterValidationOverview() {
        try {
            ValidationReport report = new ValidationReport("Report1", createOverview());
            ValidationOverview overview = report.getValidationOverview();
            Assertions.assertEquals(createOverview(), overview);

            overview.setDisclaimer("the disclaimer has been changed");

            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> report.setValidationOverview(null),
                    "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("validationOverview can not be null"), "validationOverview can not be null");
            report.setValidationOverview(overview);
            Assertions.assertEquals("the disclaimer has been changed", report.getValidationOverview().getDisclaimer());
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Getter for the field reports
     */
    @Test
    void testGetterReports() {
        try {
            ValidationReport report = new ValidationReport("Report1", createOverview());
            Assertions.assertTrue(report.getSubReports().isEmpty());
            ValidationSubReport subReport = generatePassedSubReport();
            report.addSubReport(subReport);
            Assertions.assertFalse(report.getSubReports().isEmpty(), "The list shall contain 1 item, so it can not be null");
            Assertions.assertEquals(ValidationTestResult.PASSED, report.getValidationOverallResult(), "Overall result shall be Passed");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }


    /**
     * Test : Setter for the field reports
     */
    @Covers(requirements = "VAL-38")
    @Test
    void testSetterReports() {
        try {
            ValidationReport report = new ValidationReport("Report1", createOverview());
            ValidationSubReport subReport = generatePassedSubReport();

            report.addReports(null);
            Assertions.assertTrue(report.getSubReports().isEmpty(), "shall be empty");

            List<ValidationSubReport> subReportList = new ArrayList<>();
            report.addReports(subReportList);
            Assertions.assertTrue(report.getSubReports().isEmpty(), "shall be empty");


            subReportList.add(subReport);
            subReport = generateFailedSubReport();
            subReportList.add(subReport);
            subReport = generatePassedSubReport();
            subReportList.add(subReport);
            //check list
            Assertions.assertEquals(3, subReportList.size(), "The list shall contain 3 subReports");
            report.addSubReport(generatePassedSubReport());
            report.addSubReport(generatePassedSubReport());
            report.addReports(subReportList);
            Assertions.assertEquals(5, report.getSubReports().size(), "The report shall contain 5 subReports");
            Assertions.assertEquals(ValidationTestResult.FAILED, report.getValidationOverallResult(), "The overall result shall be FAILED");

            // Check Counters
            Assertions.assertEquals(1, report.getCounters().getNumberOfErrors(), "The report shall contain 1 Error in subReports");
            Assertions.assertEquals(17, report.getCounters().getNumberOfConstraints(), "The report shall contain 17 constraint in subReports");
            Assertions.assertEquals(1, report.getCounters().getNumberOfWarnings(), "The report shall contain 1 Warning in subReports");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }


    /**
     * Test : Add subReport and check Status and Counters
     */
    @Test
    void testAddSubReport() {
        try {
            ValidationReport report = new ValidationReport("Report1", createOverview());

            // Add a Null object
            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> report.addSubReport(null),
                    "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("validateSubReport can not be null"), "validateSubReport can not be null");

            // Add a Passed SubReport
            ValidationSubReport subReport = generatePassedSubReport();
            report.addSubReport(subReport);
            Assertions.assertEquals(ValidationTestResult.PASSED, report.getValidationOverallResult(), "The overall result shall be FAILED");
            Assertions.assertEquals(0, report.getCounters().getNumberOfErrors(), "The report shall contain 0 Error in subReports");
            Assertions.assertEquals(3, report.getCounters().getNumberOfConstraints(), "The report shall contain 3 constraint in subReports");
            Assertions.assertEquals(0, report.getCounters().getNumberOfWarnings(), "The report shall contain 0 Warning in subReports");

            // Add a Failed SubReport

            subReport = generateFailedSubReport();
            report.addSubReport(subReport);
            Assertions.assertEquals(ValidationTestResult.FAILED, report.getValidationOverallResult(), "The overall result shall be FAILED");
            Assertions.assertEquals(1, report.getCounters().getNumberOfErrors(), "The report shall contain 1 Error in subReports");
            Assertions.assertEquals(8, report.getCounters().getNumberOfConstraints(), "The report shall contain 8 constraint in subReports");
            Assertions.assertEquals(1, report.getCounters().getNumberOfWarnings(), "The report shall contain 1 Warning in subReports");

            // Add an Undefined SubReport
            subReport = generateUndefinedSubReport();
            report.addSubReport(subReport);
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, report.getValidationOverallResult(), "The overall result shall be UNDEFINED");
            Assertions.assertEquals(2, report.getCounters().getNumberOfErrors(), "The report shall contain 1 Errors in subReports");
            Assertions.assertEquals(12, report.getCounters().getNumberOfConstraints(), "The report shall contain 12 constraint in subReports");
            Assertions.assertEquals(1, report.getCounters().getNumberOfWarnings(), "The report shall contain 1 Warning in subReports");

            // Undefined status shall not change
            subReport = generateFailedSubReport();
            report.addSubReport(subReport);
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, report.getValidationOverallResult(), "The overall result shall be UNDEFINED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Identity Equals
     */
    @Test
    void testIdentityEquals() {
        try {
            ValidationReport report1 = new ValidationReport("Report1", createOverview());
            report1.addSubReport(generatePassedSubReport());
            ValidationReport report2 = new ValidationReport("Report2", createOverview());
            report2.addSubReport(generateFailedSubReport());
            Assertions.assertFalse(report1.identityEquals(null), "Shall be False");
            Assertions.assertFalse(report1.identityEquals("test"), "Shall be False");
            Assertions.assertTrue(report1.identityEquals(report1), "Shall be equal");
            Assertions.assertFalse(report1.identityEquals(report2), "Shall not be equal");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : hashCode function ;
     */
    @Test
    void testHashCode() {
        try {
            ValidationReport report1 = new ValidationReport("Report1", createOverview());
            report1.addSubReport(generatePassedSubReport());
            ValidationReport report2 = new ValidationReport("Report2", createOverview());
            report2.addSubReport(generateFailedSubReport());
            Assertions.assertNotEquals(report1.hashCode(), report2.hashCode(), "Shall be different");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }


    /**
     * Test : Equals Methods and all possibilities
     */
    @Test
    void testEqualsMethod() {
        try {
            ValidationReport report1 = new ValidationReport("Report1", createOverview());
            report1.addSubReport(generatePassedSubReport());
            ValidationReport report2 = new ValidationReport("Report2", createOverview());
            report2.addSubReport(generateFailedSubReport());

            Assertions.assertFalse(report1.equals(null), "Shall be False");
            Assertions.assertEquals(report1, report1, "Shall be equal");
            Assertions.assertFalse(report1.equals("test"), "Shall be False");
            Assertions.assertNotEquals(report1, report2, "Shall not be equal");

            // case : Counters are different
            ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.MANDATORY,
                    ValidationTestResult.PASSED);
            report2 = new ValidationReport("Report2", createOverview());
            ValidationSubReport subReport = generatePassedSubReport();
            subReport.addConstraintValidation(constraintValidation);
            report2.addSubReport(subReport);
            Assertions.assertNotEquals(report1, report2, "Shall not be equal");

            report1 = new ValidationReport("Report1", createOverview());

            subReport = generatePassedSubReport();
            constraintValidation = new ConstraintValidation("test123456789", constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.RECOMMENDED,
                    ValidationTestResult.PASSED);
            subReport.addConstraintValidation(constraintValidation);
            report1.addSubReport(subReport);
            Assertions.assertNotEquals(report1, report2, "Shall not be equal");


        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Generate an Overview
     *
     * @return {@link ValidationOverview}
     */
    private ValidationOverview createOverview() {
        return new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                validationServiceVersion, validatorID);
    }

    /**
     * Create a list of Passed Constraints
     *
     * @return {@link java.util.List} object
     */
    private List<ConstraintValidation> createListConstraints() {
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.MANDATORY,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidation2 = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidation3 = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.PERMITTED,
                ValidationTestResult.PASSED);

        List<ConstraintValidation> tmp = new ArrayList<>();
        tmp.add(constraintValidation);
        tmp.add(constraintValidation2);
        tmp.add(constraintValidation3);
        return tmp;
    }

    /**
     * Generate a Passed Sub Report
     *
     * @return {@link ValidationSubReport} object
     */
    private ValidationSubReport generatePassedSubReport() {
        ValidationSubReport subReport = new ValidationSubReport("PassedSubReport", new ArrayList<>());
        subReport.setConstraints(createListConstraints());
        return subReport;
    }

    /**
     * Generate a FAILED Sub Report
     *
     * @return {@link ValidationSubReport} object
     */
    private ValidationSubReport generateFailedSubReport() {
        ValidationSubReport subReport = new ValidationSubReport("FailedSubReport", new ArrayList<>());
        subReport.setConstraints(createListConstraints());
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.FAILED);
        subReport.addConstraintValidation(constraintValidation);
        constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.MANDATORY,
                ValidationTestResult.FAILED);
        subReport.addConstraintValidation(constraintValidation);
        return subReport;
    }

    /**
     * Generate a UNDEFINED Sub Report
     *
     * @return {@link ValidationSubReport} object
     */
    private ValidationSubReport generateUndefinedSubReport() {
        ValidationSubReport subReport = new ValidationSubReport("UndefinedSubReport", new ArrayList<>());
        subReport.setConstraints(createListConstraints());
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.MANDATORY,
                ValidationTestResult.UNDEFINED);
        constraintValidation.addUnexpectedError(new IllegalArgumentException("Test exception"));
        subReport.addConstraintValidation(constraintValidation);
        return subReport;
    }

}
