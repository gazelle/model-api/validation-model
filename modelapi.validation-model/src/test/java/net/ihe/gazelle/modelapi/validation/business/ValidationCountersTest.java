package net.ihe.gazelle.modelapi.validation.business;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests
 */
class ValidationCountersTest {

    /**
     * Test ; Increment counter
     */
    @Test
    void testIncrementAndGetNumberOfConstraints() {
        try {
            ValidationCounters counters = new ValidationCounters();
            counters.incrementNumberOfConstraints();
            counters.incrementNumberOfConstraints();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();

            Assertions.assertEquals(7, counters.getNumberOfConstraints(), "Shall contain 7 constraints");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Increment Counters with Failed Infos  constraint
     */
    @Test
    void testIncrementAndGetFailedWithInfoNumber() {
        try {
            ValidationCounters counters = new ValidationCounters();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();

            Assertions.assertEquals(5, counters.getNumberOfConstraints(), "Shall contain 5 constraints");
            Assertions.assertEquals(5, counters.getFailedWithInfoNumber(), "Shall contain 5 Failed INFO");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test :  Increment counters with warning
     */
    @Test
    void testIncrementAndGetWarnings() {
        try {
            ValidationCounters counters = new ValidationCounters();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();

            Assertions.assertEquals(5, counters.getNumberOfConstraints(), "Shall contain 5 constraints");
            Assertions.assertEquals(3, counters.getNumberOfWarnings(), "Shall contain 3 WARNINGS");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Increment Counters with Error
     */
    @Test
    void testIncrementAndGetErrors() {
        try {
            ValidationCounters counters = new ValidationCounters();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfErrors();
            counters.incrementNumberOfErrors();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();

            Assertions.assertEquals(6, counters.getNumberOfConstraints(), "Shall contain 6 constraints");
            Assertions.assertEquals(2, counters.getNumberOfErrors(), "Shall contain 2 ERROR");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Add number to an existing counter
     */
    @Test
    void addNumbersFromSubCounters() {
        try {
            ValidationCounters counters = new ValidationCounters();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfErrors();
            counters.incrementNumberOfErrors();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();
            ValidationCounters subCounters = new ValidationCounters(counters);

            counters.addNumbersFromSubCounters(subCounters);

            Assertions.assertEquals(12, counters.getNumberOfConstraints(), "Shall contain 12 constraints");
            Assertions.assertEquals(4, counters.getNumberOfErrors(), "Shall contain 4 ERRORS");
            Assertions.assertEquals(4, counters.getNumberOfWarnings(), "Shall contain 4 WARNINGS");
            Assertions.assertEquals(4, counters.getFailedWithInfoNumber(), "Shall contain 4 FAILED INFOS ");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }

    }

    /**
     * Test : Compare Counters Itself, different Type, null
     */
    @Test
    void testCompareCounter() {
        try {
            ValidationCounters counters = new ValidationCounters();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfErrors();
            counters.incrementNumberOfErrors();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();

            Assertions.assertTrue(counters.equals(counters), "shall be True");
            Assertions.assertFalse(counters.equals("test"), "Shall be false");
            Assertions.assertFalse(counters.equals(null), "shall be False");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }


    /**
     * Test : Compare on the field nb of Constraints
     */
    @Test
    void testCompareNbConstraints() {
        try {
            ValidationCounters counters = new ValidationCounters();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfErrors();
            counters.incrementNumberOfErrors();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();

            ValidationCounters countersBis = new ValidationCounters();
            countersBis.incrementNumberOfWarnings();
            countersBis.incrementNumberOfErrors();
            countersBis.incrementNumberOfErrors();
            countersBis.incrementFailedWithInfoNumber();
            countersBis.incrementFailedWithInfoNumber();

            Assertions.assertNotEquals(counters, countersBis, "Shall be different due to Number of constraints");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Compare on the field nb of Failed INFO
     */
    @Test
    void testCompareNbFailedInfo() {
        try {
            ValidationCounters counters = new ValidationCounters();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfErrors();
            counters.incrementNumberOfErrors();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();

            ValidationCounters countersBis = new ValidationCounters();
            countersBis.incrementNumberOfWarnings();
            countersBis.incrementNumberOfWarnings();
            countersBis.incrementNumberOfErrors();
            countersBis.incrementFailedWithInfoNumber();
            countersBis.incrementFailedWithInfoNumber();
            countersBis.incrementFailedWithInfoNumber();

            Assertions.assertNotEquals(counters, countersBis, "Shall be different due to Number of INFO with Failed");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }


    /**
     * Test : Compare on the field nb of Warning
     */
    @Test
    void testCompareNbWarning() {
        try {
            ValidationCounters counters = new ValidationCounters();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfErrors();
            counters.incrementNumberOfErrors();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();

            ValidationCounters countersBis = new ValidationCounters();
            countersBis.incrementNumberOfWarnings();
            countersBis.incrementNumberOfWarnings();
            countersBis.incrementNumberOfWarnings();
            countersBis.incrementNumberOfErrors();
            countersBis.incrementFailedWithInfoNumber();
            countersBis.incrementFailedWithInfoNumber();

            Assertions.assertNotEquals(counters, countersBis, "Shall be different due to Number of Warning");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Compare on the field nb of Errors
     */
    @Test
    void testCompareNbError() {
        try {
            ValidationCounters counters = new ValidationCounters();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfErrors();
            counters.incrementNumberOfErrors();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();

            ValidationCounters countersBis = new ValidationCounters();
            countersBis.incrementNumberOfWarnings();
            countersBis.incrementNumberOfWarnings();
            countersBis.incrementNumberOfErrors();
            countersBis.incrementNumberOfConstraints();
            countersBis.incrementFailedWithInfoNumber();
            countersBis.incrementFailedWithInfoNumber();

            Assertions.assertNotEquals(counters, countersBis, "Shall be different due to Number of Error");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Using HashCode method
     */
    @Test
    void testHashCode() {
        try {
            ValidationCounters counters = new ValidationCounters();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfWarnings();
            counters.incrementNumberOfErrors();
            counters.incrementNumberOfErrors();
            counters.incrementFailedWithInfoNumber();
            counters.incrementFailedWithInfoNumber();

            ValidationCounters countersBis = new ValidationCounters();
            countersBis.incrementNumberOfWarnings();
            countersBis.incrementNumberOfWarnings();
            countersBis.incrementNumberOfErrors();
            countersBis.incrementNumberOfErrors();
            countersBis.incrementFailedWithInfoNumber();
            countersBis.incrementFailedWithInfoNumber();


            Assertions.assertEquals(counters.hashCode(), countersBis.hashCode(), "Shall be equal");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }

    }
}