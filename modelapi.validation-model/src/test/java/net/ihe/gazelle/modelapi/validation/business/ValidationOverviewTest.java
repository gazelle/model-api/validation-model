package net.ihe.gazelle.modelapi.validation.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

class ValidationOverviewTest {

    private static final String disclaimer = "TestValue";
    private static final String disclaimer2 = "TestValue2";
    private static final Date validationDateTime = new Date();
    private static final Date validationDateTime2 = new Date(15545);
    private static final String validationServiceName = "TestValue";
    private static final String validationServiceName2 = "TestValue2";
    private static final String validationServiceVersion = "TestValue";
    private static final String validationServiceVersion2 = "TestValue2";
    private static final String validatorID = "TestValue";
    private static final String validatorID2 = "TestValue2";
    private static final String validatorVersion = "TestValue";
    private static final String validatorVersion2 = "TestValue2";

    /**
     * Test : Test Constructors
     */
    @Test
    void testCreatValidationOverview1() {
        try {
            ValidationOverview overview = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID);
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, overview.getValidationOverallResult(), "OverallResult shall be UNDEFINED at " +
                    "initialisation");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Test Constructors
     */
    @Covers(requirements = "VAL-11")
    @Test
    void testCreatValidationOverview2() {
        try {
            ValidationOverview overview = new ValidationOverview(disclaimer, validationServiceName,
                    validationServiceVersion, validatorID, null, null);
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, overview.getValidationOverallResult(), "OverallResult shall be UNDEFINED at " +
                    "initialisation");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Test Constructors
     */
    @Test
    void testCreatValidationOverview3() {
        try {
            ValidationOverview overview = new ValidationOverview(disclaimer, validationServiceName, validationServiceVersion, validatorID);
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, overview.getValidationOverallResult(), "OverallResult shall be UNDEFINED at " +
                    "initialisation");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }


    /**
     * Test : Create without mandatory parameter
     */
    @Covers(requirements = "VAL-12")
    @Test
    void testDisclaimer() {
        try {
            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> new ValidationOverview(null
                    , validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null), "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("Disclaimer can not be null"), "Shall throw an exception if disclaimer is missing ");

            ValidationOverview overview = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            Assertions.assertEquals(disclaimer, overview.getDisclaimer(), "Shall be equal to the disclaimer");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Change Disclaimer
     */
    @Covers(requirements = "VAL-13")
    @Test
    void testChangeDisclaimer() {
        try {
            ValidationOverview overview = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            overview.setDisclaimer("TestDisclaimerChange");
            Assertions.assertEquals("TestDisclaimerChange", overview.getDisclaimer(), "Disclaimer shall have been change ");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Create without mandatory parameter
     */
    @Covers(requirements = "VAL-9")
    @Test
    void testCreatWithoutValidationDateTime() {
        try {
            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> new ValidationOverview(disclaimer
                    , null, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null), "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("ValidationDateTime can not be null"));

            ValidationOverview overview = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            Assertions.assertEquals(validationDateTime, overview.getValidationDateTime());
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Create without mandatory parameter
     */
    @Covers(requirements = "VAL-7")
    @Test
    void testCreatWithoutValidationServiceName() {
        try {
            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> new ValidationOverview(disclaimer
                    , validationDateTime, null,
                    validationServiceVersion, validatorID, validatorVersion, null), "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("validationServiceName can not be null"));

            ValidationOverview overview = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            Assertions.assertEquals(validationServiceName, overview.getValidationServiceName());
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Create without mandatory parameter
     */
    @Covers(requirements = "VAL-8")
    @Test
    void testValidationServiceVersion() {
        try {
            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> new ValidationOverview(disclaimer
                    , validationDateTime, validationServiceName,
                    null, validatorID, validatorVersion, null), "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("validationServiceVersion can not be null"), "It shall throw an exception");
            ValidationOverview overview = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            Assertions.assertEquals(validationServiceVersion, overview.getValidationServiceVersion(), "Shall be TestValue");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Create without mandatory parameter
     */
    @Covers(requirements = "VAL-10")
    @Test
    void testCreatWithoutValidatorID() {
        try {
            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> new ValidationOverview(disclaimer
                    , validationDateTime, validationServiceName,
                    validationServiceVersion, null, validatorVersion, null), "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("validatorID can not be null"), "Shall throw an exception, ValidatorID is missing");
            ValidationOverview overview = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            Assertions.assertEquals(validatorID, overview.getValidatorID());
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Try to set a null status
     */
    @Test
    void testChangeValidationStatusWithNull() {
        try {
            ValidationOverview overview = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> overview.setValidationOverallResult(null),
                    "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("validationOverallResult can not be null"), "Shall throw an exception");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Change Overall result
     */
    @Covers(requirements = "VAL-14")
    @Test
    void testChangeValidationOverallResult() {
        try {
            ValidationOverview overview = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            overview.setValidationOverallResult(ValidationTestResult.FAILED);
            Assertions.assertEquals(ValidationTestResult.FAILED, overview.getValidationOverallResult(), "Shall be set to FAILED");
            overview.setValidationOverallResult(ValidationTestResult.PASSED);
            Assertions.assertEquals(ValidationTestResult.PASSED, overview.getValidationOverallResult(), "Shall be set to PASSED");
            overview.setValidationOverallResult(ValidationTestResult.UNDEFINED);
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, overview.getValidationOverallResult(), "Shall be set to UNDEFINED");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Try to add null to Metadata
     */
    @Test
    void testAddNullObjectInMetadata() {
        try {
            ValidationOverview overview = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> overview.addAdditionalMetadata(null),
                    "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("You can not add a null Metadata in AdditionalMetadata"), "Throw an exception if" +
                    "user try to add nul object to metadata ");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Adding Metadata object
     */
    @Test
    void testMetadata() {
        try {

            ValidationOverview overview = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            overview.addAdditionalMetadata(new Metadata("test", null));
            Assertions.assertFalse(overview.getAdditionalMetadata().isEmpty(), "Shall not be empty");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Test Equal itself, different Class, null object
     */
    @Test
    void testCompareOverview() {
        try {
            //
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            Assertions.assertFalse(overviewExpected.equals(null), "Overviews shall not be equal");
            Assertions.assertFalse(overviewExpected.equals("test"), "Overviews shall not be equal");
            Assertions.assertTrue(overviewExpected.equals(overviewExpected), "Overviews shall  be equal");


        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Compare Overview on field Disclaimer
     */
    @Test
    void testCompareOverviewOnDisclaimer() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer2, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            Assertions.assertNotEquals(overviewExpected, overviewCompared, "Overviews shall not be equal due to Disclaimer");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Compare Overview on field DateTime
     */
    @Test
    void testCompareOverviewOnDateTime() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer, validationDateTime2, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            Assertions.assertNotEquals(overviewExpected, overviewCompared, "Overviews shall not be equal due to DateTime");


        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Compare Overview on field ValidationService
     */
    @Test
    void testCompareOverviewOnValidationService() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer, validationDateTime, validationServiceName2,
                    validationServiceVersion, validatorID, validatorVersion, null);
            Assertions.assertNotEquals(overviewExpected, overviewCompared, "Overviews shall not be equal due to ValidationService");


        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Compare Overview on field ValidationServiceVersion
     */
    @Test
    void testCompareOverviewOnValidationServiceVersion() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion2, validatorID, validatorVersion, null);
            Assertions.assertNotEquals(overviewExpected, overviewCompared, "Overviews shall not be equal du to ValidationServiceVersion");


        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Compare Overview on field ValidatorID
     */
    @Test
    void testCompareOverviewOnValidatorID() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID2, validatorVersion, null);
            Assertions.assertNotEquals(overviewExpected, overviewCompared, "Overviews shall not be equal due to ValidatorID");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Compare Overview on field Metadata
     */
    @Test
    void testCompareOverviewOnMetadata() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            overviewCompared.addAdditionalMetadata(new Metadata("test", null));
            Assertions.assertNotEquals(overviewExpected, overviewCompared, "Overviews shall not be equal du to Metadata ");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Compare Overview on field TestResult
     */
    @Test
    void testCompareOverviewOnTestResult() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            overviewCompared.setValidationOverallResult(ValidationTestResult.PASSED);
            Assertions.assertNotEquals(overviewExpected, overviewCompared, "Overviews shall not be equal due to TestResult ");


        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /******************************************************************
     * Test : Compare Overview on field ValidationVersion
     */
    @Test
    void testCompareOverviewOnValidatorVersion1() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion2, null);
            Assertions.assertNotEquals(overviewExpected, overviewCompared, "Overviews shall not be equal du to ValidatorVersion");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Compare Overview on field ValidationVersion
     */
    @Test
    void testCompareOverviewOnValidatorVersion2() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, null, null);
            Assertions.assertNotEquals(overviewExpected, overviewCompared, "Overviews shall not be equal");
            Assertions.assertNotEquals(overviewCompared, overviewExpected, "Overviews shall not be equal");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Compare Overview on field ValidationVersion
     */
    @Test
    void testCompareOverviewOnValidatorVersion3() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, null, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, null, null);
            Assertions.assertEquals(overviewCompared, overviewExpected, "Overviews shall  be equal");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }
    /**
     * Test : HashCodeMethod
     */
    @Test
    void testHashCodeMethodEqual() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            Assertions.assertEquals(overviewExpected.hashCode(), overviewCompared.hashCode(), "Overviews shall  be equal");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : HashCodeMethod
     */
    @Test
    void testHashCodeMethodNotEqual() {
        try {
            ValidationOverview overviewExpected = new ValidationOverview(disclaimer, validationDateTime, validationServiceName,
                    validationServiceVersion, validatorID, validatorVersion, null);
            ValidationOverview overviewCompared = new ValidationOverview(disclaimer, validationDateTime, validationServiceName2,
                    validationServiceVersion, validatorID, null, null);
            Assertions.assertNotEquals(overviewExpected.hashCode(), overviewCompared.hashCode(), "Overviews shall not be equal");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

}