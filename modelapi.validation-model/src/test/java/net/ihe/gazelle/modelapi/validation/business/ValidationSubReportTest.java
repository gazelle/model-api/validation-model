package net.ihe.gazelle.modelapi.validation.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class ValidationSubReportTest {
    private static final String NAME1 = "subReport name 1 ";
    private static final String ID1 = "ID_test1";
    private static final String constraintType1 = "length";
    private static final String constraintDescription1 = "description1 ";
    private static final String locationInValidatedObject1 = "location1";
    private static final String valueInValidatedObject1 = "value in Object";

    /**
     * Test : Create sub report and mandatory parameter
     */
    @Covers(requirements = "VAL-36")
    @Test
    void testConstructor() {
        try {
            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> new ValidationSubReport(null,
                    new ArrayList<>()), "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("Name can not be null"), "Name can not be null");

            ValidationSubReport subReport = new ValidationSubReport(NAME1, new ArrayList<>());
            Assertions.assertTrue(subReport.getStandards().isEmpty());
            subReport = new ValidationSubReport(NAME1, null);
            Assertions.assertEquals(NAME1, subReport.getName(), "Name Shall be : subReport name 1 ");
            Assertions.assertTrue(subReport.getConstraints().isEmpty(), "constraint shall be empty");
            Assertions.assertTrue(subReport.getUnexpectedErrors().isEmpty(), "Exceptions shall be empty");
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, subReport.getSubReportResult(), "subReportResult shall be Undefined");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());

        }
    }

    /**
     * Test : add null object
     */
    @Test
    void testAddNullConstraint() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);

            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> subReport.addConstraintValidation(null)
                    , "Expected to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("Constraint Validation can not be null"));
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Add  Passed Constraint and check if the subReportResult is well updated
     */
    @Test
    void testAddPassedConstraint() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);

            ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.MANDATORY,
                    ValidationTestResult.PASSED);

            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertFalse(subReport.getConstraints().isEmpty(), "constrains shall not be empty");
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "subReportResult shall be PASSED");
            constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.PERMITTED,
                    ValidationTestResult.PASSED);
            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(2, subReport.getSubCounters().getNumberOfConstraints(), "subReport shall count 2 constrains");
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "subReportResult shall be PASSED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Add  Failed Constraint and check if the subReportResult is well updated
     */
    @Covers(requirements = "VAL-27")
    @Test
    void testAddFailedMandatoryConstraint() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);

            ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.MANDATORY,
                    ValidationTestResult.FAILED);

            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertFalse(subReport.getConstraints().isEmpty(), "constrains shall not be empty");
            Assertions.assertEquals(ValidationTestResult.FAILED, subReport.getSubReportResult(), "subReportResult shall be FAILED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Add  Failed Constraint and check if the subReportResult is well updated
     */
    @Test
    void testAddFailedRecommendedConstraint() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);

            ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.RECOMMENDED,
                    ValidationTestResult.FAILED);

            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertFalse(subReport.getConstraints().isEmpty(), "constrains shall not be empty");
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "subReportResult shall be PASSED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Add  Failed Constraint and check if the subReportResult is well updated
     */
    @Test
    void testAddFailedPermittedConstraint() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);

            ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.PERMITTED,
                    ValidationTestResult.FAILED);
            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "subReportResult shall be PASSED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Add  Passed Constraints and check if the subReportResult is well updated
     */
    @Covers(requirements = "VAL-17")
    @Test
    void testAddPassedConstraints() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);
            subReport.setSubReports(null);
            subReport.setConstraints(new ArrayList<>());
            Assertions.assertEquals(0, subReport.getConstraints().size(), "constraints shall contain 0 item0 ");
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, subReport.getSubReportResult(), "subReportResult shall be UNDEFINED");
            subReport.setConstraints(createListConstraints());
            Assertions.assertEquals(3, subReport.getConstraints().size(), "constraints shall contain 3 items ");
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "subReportResult shall be PASSED");
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Add Exception to a sub report and check if subReportResult is well updated
     */
    @Covers(requirements = "VAL-15")
    @Test
    void testAddException() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);
            subReport.setConstraints(createListConstraints());
            Assertions.assertEquals(3, subReport.getConstraints().size(), "Constraints shall have 3 items");
            Assertions.assertEquals(3, subReport.getSubCounters().getNumberOfConstraints(), "NumberOfConstraints shall be 3");

            subReport.addUnexpectedError(new IllegalArgumentException("Test exception"));
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, subReport.getSubReportResult(), "subReportResult shall be UNDEFINED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Change Status from PASSED to FAILED Adding a Failed Constraint
     */
    @Covers(requirements = "VAL-39")
    @Test
    void testUpdateStatusFromPassedToFailed() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);
            subReport.setConstraints(null);
            Assertions.assertTrue(subReport.getConstraints().isEmpty(), "List shall be empty");

            subReport.setConstraints(createListConstraints());

            ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.PERMITTED,
                    ValidationTestResult.FAILED);
            subReport.addConstraintValidation(constraintValidation);

            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "SubReportResult shall have not been change and " +
                    "shall be Passed");

            constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.PERMITTED,
                    ValidationTestResult.PASSED);
            subReport.addConstraintValidation(constraintValidation);

            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "SubReportResult shall have not been change and " +
                    "shall be Passed");

            constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.RECOMMENDED,
                    ValidationTestResult.FAILED);
            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "SubReportResult shall have not been change and " +
                    "shall be Passed");

            constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.MANDATORY,
                    ValidationTestResult.FAILED);
            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(7, subReport.getConstraints().size(), "Constraints shall have 6 items");
            Assertions.assertEquals(ValidationTestResult.FAILED, subReport.getSubReportResult(), "SubReportResult shall have changed and shall be " +
                    "FAILED");


            constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.MANDATORY,
                    ValidationTestResult.PASSED);
            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(8, subReport.getConstraints().size(), "Constraints shall have 7 items");
            Assertions.assertEquals(ValidationTestResult.FAILED, subReport.getSubReportResult(), "SubReportResult shall be FAILED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Change Status from PASSED to UNDEFINED adding a Undefined constraint
     * Test : Check SubCounters
     */
    @Test
    void testUpdateStatusFromPassedToUndefined() {
        try {

            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);
            subReport.setConstraints(createListConstraints());
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "Status shall be PASSED");

            ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.PERMITTED,
                    ValidationTestResult.UNDEFINED);
            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, subReport.getSubReportResult(), "Status shall be changed from PASSED to " +
                    "UNDEFINED");

            constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.RECOMMENDED,
                    ValidationTestResult.FAILED);
            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, subReport.getSubReportResult(), "Status shall be not changed, and be UNDEFINED");


            constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.MANDATORY,
                    ValidationTestResult.PASSED);

            subReport.addConstraintValidation(constraintValidation);
            constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.MANDATORY,
                    ValidationTestResult.FAILED);

            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(7, subReport.getConstraints().size(), "Constraints shall have 7 items");
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, subReport.getSubReportResult(), "SubReportResult shall be UNDEFINED");

            // Test SubCounter
            testCounters(7, 1, 1, 0, subReport.getSubCounters());

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Add a constraint to a subReport that contain 1 subReport
     */
    @Test
    void test() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);
            subReport.addSubReport(generatePassedSubReport());

            ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.MANDATORY,
                    ValidationTestResult.PASSED);

            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertFalse(subReport.getConstraints().isEmpty(), "constrains shall not be empty");
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "subReportResult shall be PASSED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }


    /**
     * Test : change Status From Failed to Undefined
     */
    @Test
    void testUpdateStatusFromFailedToUndefined() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);
            subReport.setConstraints(createListConstraints());

            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "Status shall be PASSED");


            ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.MANDATORY,
                    ValidationTestResult.FAILED);
            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(ValidationTestResult.FAILED, subReport.getSubReportResult(), "Status shall be FAILED");

            constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.PERMITTED,
                    ValidationTestResult.UNDEFINED);
            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(5, subReport.getConstraints().size(), "Constraints shall have 5 items");
            Assertions.assertEquals(5, subReport.getSubCounters().getNumberOfConstraints(), "Constraints shall have 5 items");
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, subReport.getSubReportResult(), "Status shall be changed from FAILED to " +
                    "UNDEFINED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Add an undefined constraint to a blank subReport
     */
    @Test
    void testAddUndefinedConstraint() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);

            ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.PERMITTED,
                    ValidationTestResult.UNDEFINED);
            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, subReport.getSubReportResult(), "Status shall be UNDEFINED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Add an undefined constraint to blank subReport
     */
    @Test
    void testAddWarningConstraint() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);

            ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                    locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.RECOMMENDED,
                    ValidationTestResult.FAILED);
            subReport.addConstraintValidation(constraintValidation);
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "Status shall be PASSED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Set and  Get Exception method
     */
    @Test
    void testGetException() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);

            Assertions.assertTrue(subReport.getUnexpectedErrors().isEmpty(), "List shall be empty");
            subReport.addUnexpectedErrors(new ArrayList<>());
            Assertions.assertTrue(subReport.getUnexpectedErrors().isEmpty(), "List shall be empty");

            List<UnexpectedError> tmp = new ArrayList<>();
            tmp.add(new UnexpectedError(new IllegalArgumentException("Test exception")));
            subReport.addUnexpectedErrors(tmp);
            Assertions.assertFalse(subReport.getUnexpectedErrors().isEmpty(), "List shall not be empty");
            Assertions.assertEquals(ValidationTestResult.UNDEFINED, subReport.getSubReportResult(), "Status shall be UNDEFINED");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Add a Passed Sub Report
     */
    @Test
    void testAddPassedSubReport() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);
            subReport.setConstraints(createListConstraints());
            subReport.setSubReports(null);
            subReport.setSubReports(new ArrayList<>());
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "Status shall be PASSED");
            Assertions.assertEquals(3, subReport.getSubCounters().getNumberOfConstraints());

            IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> subReport.addSubReport(null), "Expected" +
                    " to throw, but it didn't");
            Assertions.assertTrue(thrown.getMessage().contains("validateSubReport can not be null"), "validateSubReport can not be null");

            subReport.addSubReport(generatePassedSubReport());
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "Status shall be PASSED");
            Assertions.assertEquals(1, subReport.getSubReports().size());
            Assertions.assertEquals(6, subReport.getSubCounters().getNumberOfConstraints());


        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }

    }


    /**
     * Test : Add a failed sub report
     * <p>
     * {@link ValidationSubReport} object
     */
    @Test
    void testAddFailedSubReport() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);
            subReport.setConstraints(createListConstraints());
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "Status shall be PASSED");
            testCounters(3, 0, 0, 0, subReport.getSubCounters());

            subReport.addSubReport(generateFailedSubReport());
            Assertions.assertEquals(ValidationTestResult.FAILED, subReport.getSubReportResult(), "Status shall be FAILED");
            Assertions.assertEquals(1, subReport.getSubReports().size(), "subReports shall contain 1 subReport");
            testCounters(7, 1, 0, 0, subReport.getSubCounters());

            subReport = new ValidationSubReport(NAME1, null);
            subReport.addSubReport(generateFailedSubReport());
            Assertions.assertEquals(1, subReport.getSubReports().size(), "subReports shall contain 1 subReport");
            testCounters(4, 1, 0, 0, subReport.getSubCounters());

            subReport = new ValidationSubReport(NAME1, null);
            subReport.setConstraints(createListConstraints());
            subReport.addSubReport(generatePassedSubReport());

            subReport.addSubReport(generateFailedSubReport());
            Assertions.assertEquals(ValidationTestResult.FAILED, subReport.getSubReportResult(), "Status shall be FAILED");
            Assertions.assertEquals(2, subReport.getSubReports().size(), "subReports shall contain 2 subReport");
            testCounters(10, 1, 0, 0, subReport.getSubCounters());


            subReport.addSubReport(generatePassedSubReport());
            Assertions.assertEquals(ValidationTestResult.FAILED, subReport.getSubReportResult(), "Status shall be FAILED");
            Assertions.assertEquals(3, subReport.getSubReports().size(), "subReports shall contain 3 subReport");
            testCounters(13, 1, 0, 0, subReport.getSubCounters());

            subReport.addSubReport(generateFailedSubReport());
            Assertions.assertEquals(ValidationTestResult.FAILED, subReport.getSubReportResult(), "Status shall be FAILED");


        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }

    }

    /**
     * Test : Add Undefined subReport with setSubReports
     */
    @Covers(requirements = "VAL-37")
    @Test
    void testAddUndefinedSubReport() {
        try {
            ValidationSubReport subReport = new ValidationSubReport(NAME1, null);
            subReport.setConstraints(createListConstraints());
            Assertions.assertEquals(ValidationTestResult.PASSED, subReport.getSubReportResult(), "Status shall be PASSED");
            testCounters(3, 0, 0, 0, subReport.getSubCounters());
            List<ValidationSubReport> subReportList = new ArrayList<>();
            subReportList.add(generatePassedSubReport());
            subReportList.add(generateFailedSubReport());
            subReportList.add(generateUndefinedSubReport());
            subReport.setSubReports(subReportList);

            Assertions.assertEquals(ValidationTestResult.UNDEFINED, subReport.getSubReportResult(), "Status shall be UNDEFINED");
            Assertions.assertEquals(3, subReport.getSubReports().size(), "subReports shall contain 3 subReport");
            testCounters(14, 2, 0, 0, subReport.getSubCounters());

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }


    /**
     * Test : Identity equals
     */
    @Test
    void testIdentityEquals() {
        try {
            ValidationSubReport subReport = generatePassedSubReport();
            ValidationSubReport subReport1 = generateFailedSubReport();
            Assertions.assertFalse(subReport.identityEquals(null), "Shall not be equal");
            Assertions.assertFalse(subReport.identityEquals("test"), "Shall not be equal");
            Assertions.assertTrue(subReport.identityEquals(subReport), "Shall be equal");
            Assertions.assertFalse(subReport.identityEquals(subReport1), "Shall not be equal ");

        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Implement all case of Equal methods
     * Part 1
     */
    @Test
    void testEqualsMethods() {
        try {
            ValidationSubReport subReport = generatePassedSubReport();
            ValidationSubReport subReport1 = generatePassedSubReport();

            Assertions.assertEquals(subReport.hashCode(), subReport1.hashCode(), "shall be equal");
            Assertions.assertEquals(subReport, subReport, "Shall be equal");
            Assertions.assertFalse(subReport.equals(null), "Shall be false");
            Assertions.assertFalse(subReport.equals("Test"), "shall be false");
            Assertions.assertEquals(subReport, subReport1, "Shall be equal");

            // Add Standard
            List<String> tmp = new ArrayList<>();
            tmp.add("Test");
            subReport1.setStandards(tmp);
            Assertions.assertNotEquals(subReport, subReport1, "Shall not be equal");
            subReport.setStandards(tmp);

            //Add constraint
            subReport1.setConstraints(createListConstraints());
            Assertions.assertNotEquals(subReport, subReport1, "Shall not be equal");
            subReport.setConstraints(createListConstraints());

            //Add Exception

            subReport1.addUnexpectedError(new IllegalArgumentException("Test exception"));
            subReport.addUnexpectedError(new IllegalArgumentException("Test exception 2"));
            Assertions.assertNotEquals(subReport, subReport1, "Shall not be equal");


        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Test : Implement all case of Equal methods
     * Part2
     */
    @Test
    void testEqualsMethodsPart2() {
        try {
            ValidationSubReport subReport = new ValidationSubReport("TEST", null);
            ValidationSubReport subReport1 = generatePassedSubReport();
            subReport.addSubReport(generatePassedSubReport());
            Assertions.assertNotEquals(subReport, subReport1, "Shall not be equal");

            subReport = generateFailedSubReport();
            Assertions.assertNotEquals(subReport, subReport1, "Shall not be equal");


        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }
    }

    /**
     * Create a list of Passed Constraints
     *
     * @return {@link java.util.List} object
     */
    private List<ConstraintValidation> createListConstraints() {
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.MANDATORY,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidation2 = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.RECOMMENDED,
                ValidationTestResult.PASSED);
        ConstraintValidation constraintValidation3 = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, null, null, ConstraintPriority.PERMITTED,
                ValidationTestResult.PASSED);

        List<ConstraintValidation> tmp = new ArrayList<>();
        tmp.add(constraintValidation);
        tmp.add(constraintValidation2);
        tmp.add(constraintValidation3);
        return tmp;
    }

    /**
     * Generate a Passed Sub Report
     *
     * @return {@link ValidationSubReport} object
     */
    private ValidationSubReport generatePassedSubReport() {
        ValidationSubReport subReport = new ValidationSubReport("PassedSubReport", new ArrayList<>());
        subReport.setConstraints(createListConstraints());
        return subReport;
    }

    /**
     * Generate a FAILED Sub Report
     *
     * @return {@link ValidationSubReport} object
     */
    private ValidationSubReport generateFailedSubReport() {
        ValidationSubReport subReport = new ValidationSubReport("FailedSubReport", new ArrayList<>());
        subReport.setConstraints(createListConstraints());
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.MANDATORY,
                ValidationTestResult.FAILED);
        subReport.addConstraintValidation(constraintValidation);
        return subReport;
    }

    /**
     * Generate a UNDEFINED Sub Report
     *
     * @return {@link ValidationSubReport} object
     */
    private ValidationSubReport generateUndefinedSubReport() {
        ValidationSubReport subReport = new ValidationSubReport("UndefinedSubReport", new ArrayList<>());
        subReport.setConstraints(createListConstraints());
        ConstraintValidation constraintValidation = new ConstraintValidation(ID1, constraintType1, constraintDescription1, null,
                locationInValidatedObject1, valueInValidatedObject1, new ArrayList<>(), null, ConstraintPriority.MANDATORY,
                ValidationTestResult.UNDEFINED);
        Exception exception = new IllegalArgumentException("Test exception");
        constraintValidation.addUnexpectedError(exception);
        subReport.addConstraintValidation(constraintValidation);
        return subReport;
    }

    @Covers(requirements = "VAL-29")
    void testCounters(Integer expectedConstraintNumber, Integer expectedErrorNumber, Integer expectedWarningNumber,
                      Integer expectedFailedInfoNumber, ValidationCounters counters) {
        Assertions.assertEquals(expectedConstraintNumber, counters.getNumberOfConstraints(),
                "It shall count" + expectedConstraintNumber.toString() + "constraints");
        Assertions.assertEquals(expectedFailedInfoNumber, counters.getFailedWithInfoNumber(),
                "It shall count" + expectedFailedInfoNumber.toString() + "Info with Failed");
        Assertions.assertEquals(expectedWarningNumber, counters.getNumberOfWarnings(), "It shall count" + expectedWarningNumber.toString() +
                "Warning");
        Assertions.assertEquals(expectedErrorNumber, counters.getNumberOfErrors(), "It shall count " + expectedErrorNumber.toString() + " Error");
    }
}
