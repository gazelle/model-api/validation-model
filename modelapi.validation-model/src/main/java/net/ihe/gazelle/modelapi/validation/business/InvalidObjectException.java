package net.ihe.gazelle.modelapi.validation.business;

public class InvalidObjectException extends Exception {
   public InvalidObjectException() {
   }

   public InvalidObjectException(String message) {
      super(message);
   }

   public InvalidObjectException(String message, Throwable cause) {
      super(message, cause);
   }

   public InvalidObjectException(Throwable cause) {
      super(cause);
   }
}
