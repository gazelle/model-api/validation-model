package net.ihe.gazelle.modelapi.validation.business;

public class UnkownValidatorException extends Exception {
   public UnkownValidatorException() {
   }

   public UnkownValidatorException(String message) {
      super(message);
   }

   public UnkownValidatorException(String message, Throwable cause) {
      super(message, cause);
   }

   public UnkownValidatorException(Throwable cause) {
      super(cause);
   }
}
