package net.ihe.gazelle.modelapi.validation.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * Validation report class
 *
 * @author fde
 * @version $Id : $Id
 */
public class ValidationReport implements Serializable {
    private static final long serialVersionUID = 8077553817635962645L;

    private String uuid;

    private ValidationOverview validationOverview;

    private ValidationCounters counters = new ValidationCounters();

    private List<ValidationSubReport> subReports = new ArrayList<>();

    /**
     * \ Constructor of Validation Report with Mandatory parameters
     *
     * @param uuid               {@link String}
     * @param validationOverview {@link ValidationOverview}
     */
    public ValidationReport(String uuid, ValidationOverview validationOverview) {
        setUuid(uuid);
        setValidationOverview(validationOverview);
    }

    /**
     * <p>Getter for the field <code>validationOverview</code>.</p>
     *
     * @return a {@link ValidationOverview} object.
     */
    public ValidationOverview getValidationOverview() {
        return new ValidationOverview(validationOverview);
    }

    /**
     * <p>Setter for the field <code>validationOverview</code>.</p>
     *
     * @param validationOverview a {@link ValidationOverview} object.
     *
     * @throws IllegalArgumentException if validationOverview is null
     */
    public void setValidationOverview(ValidationOverview validationOverview) {
        if (validationOverview == null) {
            throw new IllegalArgumentException("validationOverview can not be null");
        }
        this.validationOverview = validationOverview;

    }

    /**
     * <p>Getter for the field <code>uuid</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * <p>Setter for the field <code>uuid</code>.</p>
     *
     * @param uuid a {@link String} object.
     *
     * @throws IllegalArgumentException if uuid is null
     */
    public void setUuid(String uuid) {
        if (uuid == null) {
            throw new IllegalArgumentException("uuid can not be null");
        }
        this.uuid = uuid;
    }

    /**
     * <p>Getter for the field <code>reports</code>.</p>
     *
     * @return reports a {@link List} object.
     */
    public List<ValidationSubReport> getSubReports() {
        return new ArrayList<>(subReports);
    }

    /**
     * <p>Setter of the field <code>reports</code></p>
     * <p>For internal constructor usage only. cannot be used afterwards or it will mess with counters and validation status</p>
     *
     * @param subReports if reports is null, the list is initialized
     */
    private void setSubReports(Set<ValidationSubReport> subReports) {
        for (ValidationSubReport subReport : subReports) {
            addSubReport(subReport);
        }
    }

    /**
     * <p>Setter of the field <code>reports</code></p>
     *
     * @param reports if reports is null, the list is initialized
     */
    public void addReports(List<ValidationSubReport> reports) {
        if ((reports != null) && (!reports.isEmpty())) {
            reports.forEach(this::addSubReport);
        } else {
            this.subReports = new ArrayList<>();
        }
    }

    /**
     * Add a sub report to a report, it will upgrade OverallResult and Counters It will update OverallValidation Status Automatically
     *
     * @param validationSubReport {@link ValidationSubReport} can not be null
     *
     * @throws IllegalArgumentException if validationSubReport is null
     */
    public void addSubReport(ValidationSubReport validationSubReport) {
        if (validationSubReport == null) {
            throw new IllegalArgumentException("validateSubReport can not be null");
        }

        if (subReports.isEmpty()) {
            setValidationOverallResult(validationSubReport.getSubReportResult());
        } else {
            switch (validationSubReport.getSubReportResult()) {
                case FAILED:
                    setValidationOverallResult((ValidationTestResult.PASSED.equals(getValidationOverallResult())) ? ValidationTestResult.FAILED :
                            getValidationOverallResult());
                    break;
                case UNDEFINED:
                    setValidationOverallResult(ValidationTestResult.UNDEFINED);
                    break;
                default:
                    setValidationOverallResult(getValidationOverallResult());
                    break;
            }
        }
        this.subReports.add(new ValidationSubReport(validationSubReport));
        this.counters.addNumbersFromSubCounters(validationSubReport.getSubCounters());
    }

    /**
     * <p>Getter for the field <code>reports</code>.</p>
     *
     * @return reports a {@link List} object.
     */
    public ValidationTestResult getValidationOverallResult() {
        return this.validationOverview.getValidationOverallResult();
    }

    /**
     * <p>Setter of the field <code>validationStatus</code></p>
     *
     * @param testResult {@link ValidationStatus} object
     */
    private void setValidationOverallResult(ValidationTestResult testResult) {
        this.validationOverview.setValidationOverallResult(testResult);
    }

    /**
     * Getter for the field counters
     *
     * @return a {@link ValidationCounters} object
     */
    public ValidationCounters getCounters() {
        return new ValidationCounters(counters);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ValidationReport that = (ValidationReport) o;

        if (!validationOverview.equals(that.validationOverview)) {
            return false;
        }
        if (!counters.equals(that.counters)) {
            return false;
        }
        return subReports.equals(that.subReports);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = validationOverview.hashCode();
        result = 31 * result + counters.hashCode();
        result = 31 * result + subReports.hashCode();
        return result;
    }

    /**
     * Identity Equals Method
     *
     * @param o a {@link Object} object,
     *
     * @return {@link Boolean} object
     */
    public boolean identityEquals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ValidationReport that = (ValidationReport) o;
        return (uuid.equals(that.uuid));
    }

}
