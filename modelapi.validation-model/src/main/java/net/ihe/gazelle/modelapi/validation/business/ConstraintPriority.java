package net.ihe.gazelle.modelapi.validation.business;

import net.ihe.gazelle.lib.annotations.Covers;

/**
 * Priority of the Constraint
 *  if constraint is failed :
 *      Mandatory shall raise an error,
 *      Recommended shall raise a warning,
 *      Permitted shall raise an info;
 *
 *  @author fde
 *  @version $Id: $Id
 */
@Covers(requirements = "VAL-23")
public enum ConstraintPriority {
    MANDATORY,
    RECOMMENDED,
    PERMITTED
}
