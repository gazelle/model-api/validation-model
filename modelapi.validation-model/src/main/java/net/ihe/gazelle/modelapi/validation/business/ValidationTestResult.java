package net.ihe.gazelle.modelapi.validation.business;

/**
 * ValidationTestResult Enum
 *
 * @author fde
 * @version $Id : $Id
 */
public enum ValidationTestResult {
    PASSED,
    FAILED,
    UNDEFINED
}
