package net.ihe.gazelle.modelapi.validation.business;

import java.util.ArrayList;
import java.util.List;

/**
 * Carry information about available Validator in a Validation Service.
 * <p>
 * <i>A Validator can be seen as a validation profile, whereas a validation service is the engine providing the mechanism to verify the profiles.</i>
 *
 * @author ceoche
 */
public class Validator {

   private String keyword;
   private String name;
   private String domain;
   private List<String> coveredObjectTypes = new ArrayList<>();

   /**
    * Constructor of Validator with minimal <pre>keyword</pre> required parameter
    *
    * @param keyword the keyword (or id, oid) of the validator, used to programatically reference it.
    */
   public Validator(String keyword) {
      setKeyword(keyword);
   }

   /**
    * Constructor of Validator with keyword, name and domain.
    *
    * @param keyword the keyword (or id, oid) of the validator, used to programatically reference it (mandatory).
    * @param name    the human name of the validator (may be displayed in GUI)
    * @param domain  the domain which the validator is associated with (may be used to filtered the list of Validator in a Validation Service).
    */
   public Validator(String keyword, String name, String domain) {
      setKeyword(keyword);
      this.name = name;
      this.domain = domain;
   }

   /**
    * Get the keyword of the Validator
    *
    * @return the keyword of the Validator
    */
   public String getKeyword() {
      return keyword;
   }

   /**
    * Set the keyword of the Validator
    *
    * @param keyword the keyword of the Validator, must be unique per Validation Service.
    *
    * @throws IllegalArgumentException if the keyword is null or blank.
    */
   public void setKeyword(String keyword) {
      if (keyword != null && !keyword.isBlank()) {
         this.keyword = keyword.trim();
      } else {
         throw new IllegalArgumentException("Validator keyword must be defined");
      }
   }

   /**
    * Get the name of the Validator
    *
    * @return the name of the Validator or <pre>null</pre>.
    */
   public String getName() {
      return name;
   }

   /**
    * Set the name of the Validator (may be displayed in GUI).
    *
    * @param name the human name of the Validator
    */
   public void setName(String name) {
      this.name = name;
   }

   /**
    * Get the domain the Validator is associated with. This domain may be used to filtered the list of Validator in a Validation Service.
    * <p>
    * <i>It is intended that the domain of the Validator is refering to a Domain in the interoperability model of IHE (defined in GMM), but be aware
    * that some Validation Service or Test Designers may twist this field to enter other granularity level of filtering.</i>
    *
    * @return the domain the Validator is associated with or <pre>null</pre>.
    */
   public String getDomain() {
      return domain;
   }

   /**
    * Set the domain to associate with the Validator. This domain may be used to filtered the list of Validator in a Validation Service.
    *
    * @param domain the domain the validator will be associated with.
    */
   public void setDomain(String domain) {
      this.domain = domain;
   }

   /**
    * Get the list of object types covered by the Validator.
    * <p>
    * An object type is the description of the object category the object can be associated with. The most precise is the object type, the mose
    * valuable it is. As example, an object type can be an XUA SAML X-User-Assertion or an ITI-18 Find Document Request...
    * <p>
    * Object types may not be supported by Validation Services, in this case the list must be empty.
    *
    * @return the list of object types the Validator is able to cover in validation.
    */
   public List<String> getCoveredObjectTypes() {
      return new ArrayList<>(coveredObjectTypes);
   }

   /**
    * Add an object type covered by the Validator.
    *
    * @param coveredObjectType to add to the coverage of the Validator.
    *
    * @throws IllegalArgumentException if the coveredObjectType to add is null or blank
    */
   public void addCoveredObjectType(String coveredObjectType) {
      if (coveredObjectType != null && !coveredObjectType.isBlank()) {
         if (!coveredObjectTypes.contains(coveredObjectType)) {
            coveredObjectTypes.add(coveredObjectType);
         }
      } else {
         throw new IllegalArgumentException("Cannot add undefined covered ObjectType");
      }
   }

   /**
    * Remove an object type covered by the Validator.
    *
    * @param coveredObjectType to remove from the coverage of the Validator.
    *
    * @throws IllegalArgumentException if the coveredObjectType to add is null or blank
    */
   public void removeCoveredObjectType(String coveredObjectType) {
      if (coveredObjectType != null && !coveredObjectType.isBlank()) {
         coveredObjectTypes.remove(coveredObjectType);
      } else {
         throw new IllegalArgumentException("Cannot remove undefined covered ObjectType");
      }
   }

}
