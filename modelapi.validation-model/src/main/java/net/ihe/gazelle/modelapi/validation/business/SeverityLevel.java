package net.ihe.gazelle.modelapi.validation.business;

import net.ihe.gazelle.lib.annotations.Covers;

/**
 * SeverityLevel
 *  Error is set when a Mandatory constraint is failed
 *  Warning is set when a Recommended constraint is failed
 *  Info is set when a Permitted constraint is failed or a constraint is passed
 *
 * @author fde
 * @version $Id: $Id
 */
@Covers(requirements = "VAL-22")
public enum SeverityLevel {
    INFO,
    WARNING,
    ERROR
}
