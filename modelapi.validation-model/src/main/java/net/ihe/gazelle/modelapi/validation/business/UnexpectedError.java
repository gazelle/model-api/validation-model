package net.ihe.gazelle.modelapi.validation.business;

import java.io.Serializable;
import java.util.Objects;

/**
 * Exception holder for Validation Report without the stacktrace.
 */
public class UnexpectedError implements Serializable {
    private static final long serialVersionUID = -1873988319242354941L;

    private String name;
    private String message;
    private UnexpectedError cause;

    public UnexpectedError(Throwable throwable) {
        if (throwable != null) {
            setAll(throwable.getClass().getCanonicalName(),
                    throwable.getMessage(), null);
            if (throwable.getCause() != null) {
                setCause(new UnexpectedError(throwable.getCause()));
            }
        } else {
            throw new IllegalArgumentException("Cannot instantiate UnexpectedError from a null Throwable");
        }
    }

    public UnexpectedError(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public UnexpectedError(String name, String message, UnexpectedError cause) {
        setAll(name, message, cause);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null && !name.isEmpty()) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("UnexpectedError.name must be defined");
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UnexpectedError getCause() {
        return cause;
    }

    public void setCause(UnexpectedError cause) {
        this.cause = cause;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UnexpectedError)) {
            return false;
        }
        UnexpectedError that = (UnexpectedError) o;
        return name.equals(that.name) && Objects.equals(message, that.message) && Objects.equals(cause,
                that.cause);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, message, cause);
    }

    private void setAll(String name, String message,
                        UnexpectedError cause) {
        setName(name);
        setMessage(message);
        setCause(cause);
    }

}
