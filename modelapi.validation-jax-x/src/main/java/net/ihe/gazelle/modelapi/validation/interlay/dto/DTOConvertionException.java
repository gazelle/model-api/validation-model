package net.ihe.gazelle.modelapi.validation.interlay.dto;

public class DTOConvertionException extends RuntimeException {

    private static final long serialVersionUID = -6524902612588354557L;

    public DTOConvertionException() {
    }

    public DTOConvertionException(String s) {
        super(s);
    }

    public DTOConvertionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DTOConvertionException(Throwable throwable) {
        super(throwable);
    }
}
