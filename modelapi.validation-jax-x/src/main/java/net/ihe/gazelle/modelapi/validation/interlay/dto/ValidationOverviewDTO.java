package net.ihe.gazelle.modelapi.validation.interlay.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.ihe.gazelle.modelapi.validation.business.Metadata;
import net.ihe.gazelle.modelapi.validation.business.ValidationOverview;
import net.ihe.gazelle.modelapi.validation.business.ValidationTestResult;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

@Schema(description = "Résumé du rapport de validation.")
@XmlRootElement(name = "validationOverview")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder = {
        "disclaimer",
        "validationServiceName",
        "validationServiceVersion",
        "validatorID",
        "validatorVersion",
        "additionalMetadata"
})
public class ValidationOverviewDTO implements DataTransferObject<ValidationOverview> {

    private ValidationTestResult validationOverallResult;
    private String disclaimer;
    private XMLGregorianCalendar validationDateTime;
    private String validationServiceName;
    private String validationServiceVersion;
    private String validatorID;
    private String validatorVersion;
    private List<MetadataDTO> additionalMetadataList;

    public ValidationOverviewDTO(ValidationOverview domain, List<Metadata> additionalMetadata) {
        this.validationOverallResult = domain.getValidationOverallResult();
        this.disclaimer = domain.getDisclaimer();
        this.validationDateTime = mapValidationDateTime(domain.getValidationDateTime());
        this.validationServiceName = domain.getValidationServiceName();
        this.validationServiceVersion = domain.getValidationServiceVersion();
        this.validatorID = domain.getValidatorID();
        this.validatorVersion = domain.getValidatorVersion();
        this.additionalMetadataList =
                domain.getAdditionalMetadata() != null
                        ? mapAdditionalMetadata(domain.getAdditionalMetadata())
                        : null;
        if (additionalMetadata != null) {
            if (additionalMetadataList == null) {
                additionalMetadataList = mapAdditionalMetadata(additionalMetadata);
            } else {
                additionalMetadataList.addAll(
                        mapAdditionalMetadata(additionalMetadata)
                );
            }
        }
    }

    public ValidationOverviewDTO() {
    }

    @Schema(name = "validationOverallResult", description = "") // TODO
    @JsonProperty("validationOverallResult")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/", name = "validationOverallResult")
    public ValidationTestResult getValidationOverallResult() {
        return validationOverallResult;
    }

    public void setValidationOverallResult(
            ValidationTestResult validationOverallResult) {
        this.validationOverallResult = validationOverallResult;
    }

    @Schema(name = "disclaimer", description = "") // TODO
    @JsonProperty("disclaimer")
    @XmlElement(name = "disclaimer")
    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    @Schema(name = "validationDateTime", description = "") // TODO
    @JsonProperty("validationDateTime")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/", name = "validationDateTime")
    public XMLGregorianCalendar getValidationDateTime() {
        return validationDateTime;
    }

    public void setValidationDateTime(XMLGregorianCalendar validationDateTime) {
        this.validationDateTime = validationDateTime;
    }

    @Schema(name = "validationServiceName", description = "") // TODO
    @JsonProperty("validationServiceName")
    @XmlElement(name = "validationServiceName")
    public String getValidationServiceName() {
        return validationServiceName;
    }

    public void setValidationServiceName(String validationServiceName) {
        this.validationServiceName = validationServiceName;
    }

    @Schema(name = "validationServiceVersion", description = "") // TODO
    @JsonProperty("validationServiceVersion")
    @XmlElement(name = "validationServiceVersion")
    public String getValidationServiceVersion() {
        return validationServiceVersion;
    }

    public void setValidationServiceVersion(String validationServiceVersion) {
        this.validationServiceVersion = validationServiceVersion;
    }

    @Schema(name = "validatorID", description = "") // TODO
    @JsonProperty("validatorID")
    @XmlElement(name = "validatorID")
    public String getValidatorID() {
        return validatorID;
    }

    public void setValidatorID(String validatorID) {
        this.validatorID = validatorID;
    }

    @Schema(name = "validatorVersion", description = "") // TODO
    @JsonProperty("validatorVersion")
    @XmlElement(name = "validatorVersion")
    public String getValidatorVersion() {
        return validatorVersion;
    }

    public void setValidatorVersion(String validatorVersion) {
        this.validatorVersion = validatorVersion;
    }

    @Schema(name = "additionalMetadata", description = "") // TODO
    @JsonProperty("additionalMetadata")
    @XmlElement(name = "additionalMetadata")
    public List<MetadataDTO> getAdditionalMetadata() {
        return additionalMetadataList;
    }

    public void setAdditionalMetadata(
            List<MetadataDTO> additionalMetadataDTOs) {
        this.additionalMetadataList = additionalMetadataDTOs;
    }

    @Override
    public ValidationOverview toDomain() {
        ValidationOverview validationOverview = new ValidationOverview(disclaimer,
                validationDateTime != null ? validationDateTime.toGregorianCalendar().getTime() : null,
                validationServiceName,
                validationServiceVersion,
                validatorID);
        validationOverview.setValidatorVersion(validatorVersion);
        if (additionalMetadataList != null) {
            for (MetadataDTO metadataDTO : additionalMetadataList) {
                validationOverview.addAdditionalMetadata(metadataDTO.toDomain());
            }
        }
        return validationOverview;
    }

    private List<MetadataDTO> mapAdditionalMetadata(List<Metadata> additionalMetadata) {
        List<MetadataDTO> metadataDTOs = new ArrayList<>();
        for (Metadata metadata : additionalMetadata) {
            metadataDTOs.add(new MetadataDTO(metadata));
        }
        return metadataDTOs;
    }

    private XMLGregorianCalendar mapValidationDateTime(Date validationDateTime) {
        if (validationDateTime != null) {
            GregorianCalendar gCalendar = new GregorianCalendar();
            gCalendar.setTimeZone(TimeZone.getTimeZone("GMT"));
            gCalendar.setTime(validationDateTime);
            try {
                return DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
            } catch (DatatypeConfigurationException e) {
                throw new DTOConvertionException(e);
            }
        } else {
            return null;
        }
    }

}
