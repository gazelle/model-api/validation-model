package net.ihe.gazelle.modelapi.validation.interlay.ws.rest;

import net.ihe.gazelle.modelapi.validation.business.InvalidObjectException;
import net.ihe.gazelle.modelapi.validation.business.UnkownValidatorException;
import net.ihe.gazelle.modelapi.validation.business.Validator;
import net.ihe.gazelle.modelapi.validation.interlay.dto.ValidationReportDTO;

import java.util.List;

/**
 * Validation Service API.
 * <p>
 * Interact with a validation service, get information on available validators, supported object-types and media-types and trigger a validation.
 * <p>
 * <i>A Validator can be seen as a validation profile, whereas a validation service is the engine providing the mechanism to verify the profile.</i>
 *
 * @author ceoche
 */
public interface ValidationServiceDTO {

    /**
     * Get the name of the Validation Service
     *
     * @return the name of the Validation Service
     */
    String getName();

    /**
     * Get the list of available Validators
     *
     * @return list of available validators
     */
    List<Validator> getValidators();

    /**
     * Get the list of available Validators filtered by a domain (or a disciminator string)
     *
     * @param domain to filter Validators list on.
     *
     * @return the list of validators that match the given domain filter.
     */
    List<Validator> getValidators(String domain);

    /**
     * Get the list of supported media-types (MIME Types of the objects to validate). <i>Media types returned should be part of the official IANA
     * list.</i>
     *
     * @return the list of Media-Types the Validation Service is supporting.
     *
     * @see <a href="https://www.iana.org/assignments/media-types/media-types.xhtml">https://www.iana.org/assignments/media-types/media-types.xhtml</a>
     */
    List<String> getSupportedMediaTypes();

    /**
     * Validate an object according to the requested Validator and get a detailed report. The validator will try to perform all decoding and
     * verifications, even if one is failed, to return the most complete report possible.
     *
     * @param object           the object to validate in binary (document, message, archive or ressource, that may depends on the validation service).
     * @param validatorKeyword the keyword of the validator to use to verify the object.
     *
     * @return A detailed {@link ValidationReportDTO}.
     *
     * @throws UnkownValidatorException if the requested Validator is not known nor available in the Validation Service
     */
    ValidationReportDTO validate(byte[] object, String validatorKeyword) throws UnkownValidatorException, InvalidObjectException;

}
