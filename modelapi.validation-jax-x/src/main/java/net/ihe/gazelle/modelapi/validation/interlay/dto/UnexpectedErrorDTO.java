package net.ihe.gazelle.modelapi.validation.interlay.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.ihe.gazelle.modelapi.validation.business.UnexpectedError;

@Schema(name = "unexpectedError", description = "Error catched during valiation")
@XmlRootElement(name = "unexpectedError")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder = {"message", "cause"})
public class UnexpectedErrorDTO implements DataTransferObject<UnexpectedError> {

    private String name;
    private String message;
    private UnexpectedErrorDTO cause;

    public UnexpectedErrorDTO(UnexpectedError domain) {
        this.name = domain.getName();
        this.message = domain.getMessage();
        this.cause = domain.getCause() != null ? new UnexpectedErrorDTO(domain.getCause()) : null;
    }

    public UnexpectedErrorDTO() {
        super();
    }

    @Schema(name = "name", description = "") // TODO
    @JsonProperty("name")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/",name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Schema(name = "message", description = "") // TODO
    @JsonProperty("message")
    @XmlElement(name = "message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Schema(name = "cause", description = "") //TODO
    @JsonProperty("cause")
    @XmlElement(name = "cause")
    public UnexpectedErrorDTO getCause() {
        return cause;
    }

    public void setCause(UnexpectedErrorDTO cause) {
        this.cause = cause;
    }

    @Override
    public UnexpectedError toDomain() {
        return toDomain(this);
    }

    private UnexpectedError toDomain(UnexpectedErrorDTO unexpectedErrorDTO) {
        if (unexpectedErrorDTO.getCause() != null) {
            return new UnexpectedError(
                    unexpectedErrorDTO.getName(),
                    unexpectedErrorDTO.getMessage(),
                    toDomain(unexpectedErrorDTO.getCause()));
        } else {
            return new UnexpectedError(unexpectedErrorDTO.getName(), unexpectedErrorDTO.getMessage());
        }
    }
}
