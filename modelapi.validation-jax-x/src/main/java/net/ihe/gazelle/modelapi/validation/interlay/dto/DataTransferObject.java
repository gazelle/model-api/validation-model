package net.ihe.gazelle.modelapi.validation.interlay.dto;

public interface DataTransferObject<T> {
    T toDomain();
}
