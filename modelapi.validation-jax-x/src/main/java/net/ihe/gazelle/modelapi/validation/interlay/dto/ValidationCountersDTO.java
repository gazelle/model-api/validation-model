package net.ihe.gazelle.modelapi.validation.interlay.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import net.ihe.gazelle.modelapi.validation.business.ValidationCounters;

@Schema(description = "Compteurs du rapport de validation.")
@XmlRootElement(name = "validationCounters")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ValidationCountersDTO {

    private Integer numberOfConstraints;
    private Integer failedWithInfoNumber;
    private Integer numberOfWarnings;
    private Integer numberOfErrors;

    public ValidationCountersDTO(ValidationCounters domain) {
        numberOfConstraints = domain.getNumberOfConstraints();
        failedWithInfoNumber = domain.getFailedWithInfoNumber();
        numberOfWarnings = domain.getNumberOfWarnings();
        numberOfErrors = domain.getNumberOfErrors();
    }

    public ValidationCountersDTO() {

    }

    @Schema(name = "numberOfConstraints", description = "") // TODO
    @JsonProperty("numberOfConstraints")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/",name = "numberOfConstraints")
    public Integer getNumberOfConstraints() {
        return numberOfConstraints;
    }

    public void setNumberOfConstraints(Integer numberOfConstraints) {
        this.numberOfConstraints = numberOfConstraints;
    }

    @Schema(name = "failedWithInfoNumber", description = "") // TODO
    @JsonProperty("failedWithInfoNumber")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/",name = "failedWithInfoNumber")
    public Integer getFailedWithInfoNumber() {
        return failedWithInfoNumber;
    }

    public void setFailedWithInfoNumber(Integer failedWithInfoNumber) {
        this.failedWithInfoNumber = failedWithInfoNumber;
    }

    @Schema(name = "numberOfWarnings", description = "") // TODO
    @JsonProperty("numberOfWarnings")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/",name = "numberOfWarnings")
    public Integer getNumberOfWarnings() {
        return numberOfWarnings;
    }

    public void setNumberOfWarnings(Integer numberOfWarnings) {
        this.numberOfWarnings = numberOfWarnings;
    }

    @Schema(name = "numberOfErrors", description = "") // TODO
    @JsonProperty("numberOfErrors")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/",name = "numberOfErrors")
    public Integer getNumberOfErrors() {
        return numberOfErrors;
    }

    public void setNumberOfErrors(Integer numberOfErrors) {
        this.numberOfErrors = numberOfErrors;
    }
}
