package net.ihe.gazelle.modelapi.validation.interlay.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;
import net.ihe.gazelle.modelapi.validation.business.Metadata;

@Schema(description = "Métadonnée du rapport de validation.")
@XmlRootElement(name = "metadata")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class MetadataDTO implements DataTransferObject<Metadata> {

    private String name;
    private String value;

    public MetadataDTO(Metadata domain) {
        name = domain.getName();
        value = domain.getValue();
    }

    public MetadataDTO() {
    }

    @Schema(name = "name", description = "") // TODO
    @JsonProperty("name")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/",name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Schema(name = "value", description = "") // TODO
    @JsonProperty("value")
    @XmlValue
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public Metadata toDomain() {
        return new Metadata(name, value);
    }
}
