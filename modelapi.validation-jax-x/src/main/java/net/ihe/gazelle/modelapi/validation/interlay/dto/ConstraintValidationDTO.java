package net.ihe.gazelle.modelapi.validation.interlay.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.modelapi.validation.business.ConstraintPriority;
import net.ihe.gazelle.modelapi.validation.business.ConstraintValidation;
import net.ihe.gazelle.modelapi.validation.business.SeverityLevel;
import net.ihe.gazelle.modelapi.validation.business.UnexpectedError;
import net.ihe.gazelle.modelapi.validation.business.ValidationTestResult;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@Schema(description = "Contrainte du rapport de validation.")
@XmlRootElement(name = "constraintValidation")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ConstraintValidationDTO implements DataTransferObject<ConstraintValidation> {

    private String constraintID;
    private String constraintType;
    private String constraintDescription;
    private String formalExpression;
    private String locationInValidatedObject;
    private String valueInValidatedObject;
    private List<String> assertionIDs;
    private List<UnexpectedErrorDTO> unexpectedErrors;
    private ValidationTestResult testResult;
    private SeverityLevel severity;
    private ConstraintPriority priority;

    public ConstraintValidationDTO(ConstraintValidation domain) {
        constraintID = domain.getConstraintID();
        constraintType = domain.getConstraintType();
        constraintDescription = domain.getConstraintDescription();
        formalExpression = domain.getFormalExpression();
        locationInValidatedObject = domain.getLocationInValidatedObject();
        valueInValidatedObject = domain.getValueInValidatedObject();
        assertionIDs = domain.getAssertionIDs() != null ? new ArrayList<>(domain.getAssertionIDs()) : null;
        unexpectedErrors = domain.getUnexpectedErrors() != null ? mapExceptions(domain.getUnexpectedErrors()) : null;
        testResult = domain.getTestResult();
        severity = domain.getSeverity();
        priority = domain.getPriority();
    }

    public ConstraintValidationDTO() {
        super();
    }

    @Schema(name = "constraintID", description = "") // TODO
    @JsonProperty("constraintID")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/",name = "constraintID")
    public String getConstraintID() {
        return constraintID;
    }

    public void setConstraintID(String constraintID) {
        this.constraintID = constraintID;
    }

    @Schema(name = "constraintType", description = "") // TODO
    @JsonProperty("constraintType")
    @XmlElement(name = "constraintType")
    public String getConstraintType() {
        return constraintType;
    }

    public void setConstraintType(String constraintType) {
        this.constraintType = constraintType;
    }

    @Schema(name = "constraintDescription", description = "") // TODO
    @JsonProperty("constraintDescription")
    @XmlElement(name = "constraintDescription")
    public String getConstraintDescription() {
        return constraintDescription;
    }

    public void setConstraintDescription(String constraintDescription) {
        this.constraintDescription = constraintDescription;
    }

    @Schema(name = "formalExpression", description = "") // TODO
    @JsonProperty("formalExpression")
    @XmlElement(name = "formalExpression")
    public String getFormalExpression() {
        return formalExpression;
    }

    public void setFormalExpression(String formalExpression) {
        this.formalExpression = formalExpression;
    }

    @Schema(name = "locationInValidatedObject", description = "") // TODO
    @JsonProperty("locationInValidatedObject")
    @XmlElement(name = "locationInValidatedObject")
    public String getLocationInValidatedObject() {
        return locationInValidatedObject;
    }

    public void setLocationInValidatedObject(String locationInValidatedObject) {
        this.locationInValidatedObject = locationInValidatedObject;
    }

    @Schema(name = "valueInValidatedObject", description = "") // TODO
    @JsonProperty("valueInValidatedObject")
    @XmlElement(name = "valueInValidatedObject")
    public String getValueInValidatedObject() {
        return valueInValidatedObject;
    }

    public void setValueInValidatedObject(String valueInValidatedObject) {
        this.valueInValidatedObject = valueInValidatedObject;
    }

    @Schema(name = "assertionID", description = "") // TODO
    @JsonProperty("assertionID")
    @XmlElement(name = "assertionID")
    public List<String> getAssertionIDs() {
        return assertionIDs;
    }

    public void setAssertionIDs(List<String> assertionIDs) {
        this.assertionIDs = assertionIDs;
    }

    @Schema(name = "testResult", description = "") // TODO
    @JsonProperty("testResult")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/",name = "testResult")
    public ValidationTestResult getTestResult() {
        return testResult;
    }

    public void setTestResult(ValidationTestResult testResult) {
        this.testResult = testResult;
    }

    @Schema(name = "severity", description = "") // TODO
    @JsonProperty("severity")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/",name = "severity")
    public SeverityLevel getSeverity() {
        return severity;
    }

    public void setSeverity(SeverityLevel severity) {
        this.severity = severity;
    }

    @Schema(name = "priority", description = "") // TODO
    @JsonProperty("priority")
    @XmlAttribute(namespace = "http://validationreport.gazelle.ihe.net/",name = "priority")
    public ConstraintPriority getPriority() {
        return priority;
    }

    public void setPriority(ConstraintPriority priority) {
        this.priority = priority;
    }

    @Schema(name = "unexpectedErrors", description = "") // TODO
    @JsonProperty("unexpectedErrors")
    @XmlElement(name = "unexpectedError")
    public List<UnexpectedErrorDTO> getUnexpectedErrors() {
        return unexpectedErrors;
    }

    public void setUnexpectedErrors(List<UnexpectedErrorDTO> unexpectedErrors) {
        this.unexpectedErrors = unexpectedErrors;
    }

    @Override
    public ConstraintValidation toDomain() {
        List<UnexpectedError> unexpectedErrorsDomain = new ArrayList<>();
        if (unexpectedErrors != null) {
            for (UnexpectedErrorDTO exceptionDTO : unexpectedErrors) {
                unexpectedErrorsDomain.add(exceptionDTO.toDomain());
            }
        }
        return new ConstraintValidation(
                constraintID,
                constraintType,
                constraintDescription,
                formalExpression,
                locationInValidatedObject,
                valueInValidatedObject,
                assertionIDs,
                unexpectedErrorsDomain,
                priority,
                testResult);
    }

    private List<UnexpectedErrorDTO> mapExceptions(List<UnexpectedError> exceptions) {
        List<UnexpectedErrorDTO> exceptionDTOS = new ArrayList<>();
        for (UnexpectedError throwable : exceptions) {
            exceptionDTOS.add(new UnexpectedErrorDTO(throwable));
        }
        return exceptionDTOS;
    }


}
